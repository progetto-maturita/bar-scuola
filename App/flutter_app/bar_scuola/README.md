# bar_scuola

Applicazione per gestire il bar della scuola

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## Prime prove
Denis: Sto seguendo questo tutorial: https://flutter.dev/docs/get-started/codelab
su come scrivere la prima app con flutter

## Altre prove
Denis: Sto seguendo il tutorial: https://codelabs.developers.google.com/codelabs/first-flutter-app-pt2
su come continuare la app con le parole per imaprare come funziona flutter

