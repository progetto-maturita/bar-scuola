import 'package:bar_scuola/constants.dart';
import 'package:bar_scuola/pagine/main/LoginPage.dart';
import 'package:flutter/material.dart';

class topNavigation extends StatefulWidget {
  const topNavigation({
    Key key,
  }) : super(key: key);

  @override
  _topNavigation createState() => _topNavigation();
}

class _topNavigation extends State<topNavigation> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 1000),
        child: Container(
          color: kPrimaryColor,
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Text('Bar Severi', style: TextStyle(color: kSecondaryColor)),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {},
                        child: Text(
                          'Scopri i nostri prodotti',
                          style: TextStyle(color: kSecondaryColor),
                        ),
                      ),
                      SizedBox(width: screenSize.width / 20),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Text(
                    'Registrati',
                    style: TextStyle(color: kSecondaryColor),
                  ),
                ),
                SizedBox(
                  width: screenSize.width / 50,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(color: kSecondaryColor),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(),
    );
  }
}
