import 'package:bar_scuola/pagine/main/navigazioneMain.dart';
import 'package:bar_scuola/responsive.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // It provide us the width and height
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      body: Responsive(
        // Let's work on our mobile part
        mobile: navigazioneMain(),
        tablet: Row(
          children: [
            Expanded(
              child: navigazioneMain(),
            ),
            /*Expanded(
              flex: 9,
              child: navigazioneMain(),
            ),*/
          ],
        ),
        desktop: Row(
          children: [
            // Once our width is less then 1300 then it start showing errors
            // Now there is no error if our width is less then 1340
            Expanded(
              child: navigazioneMain(),
            ),
            /*Expanded(
              flex: _size.width > 1340 ? 3 : 5,
              child: navigazioneMain(),
            ),
            Expanded(
              flex: _size.width > 1340 ? 8 : 10,
              child: navigazioneMain(),
            ),*/
          ],
        ),
      ),
    );
  }
}
