import 'package:bar_scuola/components/bottomNavigation.dart';
import 'package:bar_scuola/components/topNavigation.dart';
import 'package:bar_scuola/responsive.dart';
import 'package:flutter/material.dart';

class navigazioneMain extends StatefulWidget {
  const navigazioneMain({
    Key key,
  }) : super(key: key);

  @override
  _navigazioneMainState createState() => _navigazioneMainState();
}

class _navigazioneMainState extends State<navigazioneMain> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Responsive(
      mobile: bottomNavigation(),
      tablet: topNavigation(),
      desktop: topNavigation(),
    ));
  }
}
