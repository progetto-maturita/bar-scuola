<?php

require_once "generale.php";
require_once "credenziali.php";

$connessione = new mysqli($server, $db_username, $db_password, $db_name);

if($connessione->connect_error){
  die(sendJsonError("Generico", 'Errore connessione DB'));
}

$connessione->set_charset("utf8mb4_unicode_ci");

?>
