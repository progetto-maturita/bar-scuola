<?php
require_once "generale.php";

if (isset($_POST['username']) && isset($_POST['password'])) {
  if(!maybeStartSession()){
    session_start();
  }

  if( !validateLogin($_POST['username']) ){
    sendJsonError("Username", "Login non valido");
  }

  if(logged()){
    sendJsonSuccess([
          'username' => username(),
          'permessoUtente' => permessoUtente(),
          'sessionId' => session_id(),
      ],
      false
    );
  }

  require_once "connessioneDB.php";

  $login = htmlentities(substr($_POST['username'], 0, 64));
  $password = substr($_POST['password'], 0, 255);

  $login_type = strpos($login, '@') !== FALSE ? 'Email' : 'Username';
  $sql_where  = " $login_type = ?";

  $stmt = $connessione->prepare("SELECT * FROM utente WHERE $sql_where");
  $stmt->bind_param("s", $login);
  $result = $stmt->execute();

  if($result !== FALSE){

    $result = $stmt->get_result();

    if($result->num_rows > 0){

      $result = $result->fetch_assoc();

      if(password_verify($password, $result['Password']) || verificaPassword($password, $result['Password'])){

        $_SESSION['username']  = htmlspecialchars($result['Username']);
        $_SESSION['permessoUtente'] = htmlspecialchars($result['Permesso']);

      }else{
        sendJsonError("Password-Utente", "Password invalida");
      }
    }else{
      sendJsonError("Password-Utente", "Utente invalido");
    }
  }

  chiudiConnessione();

  if(logged()){
    sendJsonSuccess([
        'username' => username(),
        'permessoUtente' => permessoUtente(),
        'sessionId' => session_id(),
      ],
      false
    );
  }else{
    sendJsonError("Generico", "Problema generico login");
  }
}
/*

debug
http://127.0.0.1:8080/api_php/login.php?username=q&password=q

*/

?>
