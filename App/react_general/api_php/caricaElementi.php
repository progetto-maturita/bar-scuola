<?php
require_once "generale.php";

maybeStartSession();

require_once "connessioneDB.php";

$stmt = $connessione->prepare("SELECT * FROM prodotto WHERE ID = 0");
$result = $stmt->execute();

if($result !== FALSE){

$result = $stmt->get_result();

if($result->num_rows > 0){

$result = $result->fetch_assoc();

if(password_verify($password, $result['Password']) || verificaPassword($password, $result['Password'])){

$_SESSION['username']  = htmlspecialchars($result['Username']);
$_SESSION['permessoUtente'] = htmlspecialchars($result['Permesso']);

}else{
sendJsonError("Password-Utente", "Password invalida");
chiudiConnessioneEDie();
}
}else{
sendJsonError("Password-Utente", "Utente invalido");
chiudiConnessioneEDie();
}
}

chiudiConnessione();

if(logged()){
sendJsonSuccess([
'username' => username(),
'permessoUtente' => permessoUtente(),
'sessionId' => session_id(),
],
false
);
}else{
sendJsonError("Generale", "Problema login");
}
chiudiConnessioneEDie();
}
/*

debug
http://127.0.0.1:8080/api_php/login.php?username=q&password=q

*/

?>
