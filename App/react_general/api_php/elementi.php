<?php
require_once "generale.php";

maybeStartSession();

require_once "connessioneDB.php";

$stmt = $connessione->prepare("SELECT * FROM famiglia");
$result = $stmt->execute();

if($result !== FALSE){

  $result = $stmt->get_result();

  if($result->num_rows > 0){

    $elementi = [];
    while($row = $result->fetch_assoc()){
      $elementi[] = [
        'ID'		      => htmlspecialchars($row['ID']),
        'Descrizione'  => htmlspecialchars($row['Descrizione']),
        'Categoria' 	    => htmlspecialchars($row['Categoria'])
      ];
    }
    if(! empty($elementi) && count($elementi) > 0){
      sendJsonSuccess($elementi, false);
    }else{
      sendJsonSuccess();
    }
  }
}

/*

debug
http://127.0.0.1:8080/api_php/login.php?username=q&password=q

*/

?>
