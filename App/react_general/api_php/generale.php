<?php

/*Sicuramente molto spazio per miglioramenti

*/

  require_once 'debug.php';

  $nomeReg = "/^(([A-Za-z]+[-']?)*([A-Za-z]+)\s?)+([A-Za-z]+[-']?)*([A-Za-z]+)?$/";
  $cognomeReg = "/^(([A-Za-z]+[-']?)*([A-Za-z]+)\s?)+([A-Za-z]+[-']?)*([A-Za-z]+)?$/";
  $emailReg = "/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/";
  $usernameReg = "/^[a-zA-Z0-9!_+\-*#$^.]+$/";
  $strongReg = "/^(?=.*[A-Z])(?=.*[-_<>!@#$%^&*.])(?=.*[0-9])(?=.*[a-z]).{8,64}$/";
  $mediumReg = "/^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,64}$/";

  /*https://stackoverflow.com/questions/44479681/cors-php-response-to-preflight-request-doesnt-pass-am-allowing-origin
  https://www.dinochiesa.net/?p=754
  */

  $sess_id = FALSE;
  if(!defined('NO_REACT_HEADERS')){
    /*ini_set('session.cookie_secure', "1");
    ini_set('session.cookie_httponly', "1");
    ini_set('session.cookie_samesite', 'None');*/
    header('Access-Control-Allow-Origin: ' . (defined('DEBUG') ? '*' : ($_SERVER['HTTP_ORIGIN'] ?? $_SERVER['HTTP_REFERER'] ?? '*')));
    header('Access-Control-Allow-Credentials: true');
    header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type");
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))

          header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
          header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }
    $rest_json = file_get_contents("php://input");
    if(isset($rest_json) && strlen($rest_json) > 0){
      $_POST = json_decode($rest_json, true);
    }elseif(defined('DEBUG')){
      $_POST = $_REQUEST;
    }

  }

  $sess_id = isset($_POST['sessionId']) ? ($_POST['sessionId'] != "" ? $_POST['sessionId'] : FALSE) : FALSE;
  if($sess_id){
    session_id($sess_id);
  }

  function maybeStartSession(){
    global $sess_id;
    if (
      $sess_id &&
      session_status() === PHP_SESSION_NONE
    ) {
      session_start();
      return true;
    }
    return false;
  }

  function checkToken($required){
    if (isset($_POST['token'])) {
      if($_POST['token'] != "None" && isset($_SESSION['permessoUtente']) && isset($_SESSION['username'])){
        if($_POST['token'] != $_SESSION['token']){
          sendJsonError("Generico", "CSRF non valido");
          chiudiConnessioneEDie();
        }
      }else{
        if($required && $_POST['token'] == "None" && isset($_SESSION['permessoUtente']) && isset($_SESSION['username'])){
          sendJsonError("Generico", "CSRF non valido");
          chiudiConnessioneEDie();
        }
      }
    }else{
      if($required){
        sendJsonError("Generico", "CSRF non valido");
        chiudiConnessioneEDie();
      }
    }


  }

  function chiudiConnessione(){
    if(isset($connessione)){
      $connessione->close();
    }
  }

  function chiudiConnessioneEDie(){
    chiudiConnessione();
    die();
  }

  function generateToken(){
    $token = "None";
    if(isset($_SESSION['permessoUtente']) && isset($_SESSION['username'])){
      $token = openssl_random_pseudo_bytes(16);
      $token = bin2hex($token);
      $_SESSION['token'] = $token;
    }
    return $token;
  }

  function arrayHtmlspecialchars($array){
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = htmlspecialchars($item);
        }
    });

    return $array;
  }

  function sendJsonError($msg, $adv_msg = 'None'){
    echo json_encode([
      'success' => false,
      'CSRF' => isset($_SESSION['username']) ? generateToken() : "None",
      'data' => [
        'msg' => htmlspecialchars($msg),
        'avanced' => (debug()) ? htmlspecialchars($adv_msg) : "Production",
      ]
    ]);
    chiudiConnessioneEDie();
  }

  function sendJsonSuccess($data = 'None', bool $encode = true){
    echo json_encode([
      'success' => true,
      'CSRF' => isset($_SESSION['username']) ? generateToken() : "None",
      'data' =>
        ($encode === TRUE) ? (
          is_array($data) ? arrayHtmlspecialchars($data) : htmlspecialchars($data)
        ) : $data,
    ]);
    chiudiConnessioneEDie();
  }

  function debug(){
      if(defined('DEBUG')){
        return true;
      }
      return false;
  }

  function verificaPassword($password, $resultVer){

    if(defined('DEBUG')){

      if($password == $resultVer){
        return true;
      }

      return false;

    }

    return false;

  }

  function logged(){
    return session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['username']);
  }

  function permessoUtente(){

    if( !logged() ){
      return false;
    }

    return $_SESSION['permessoUtente'];
  }

  function username(){
    if( !logged() ){
      return false;
    }

    return $_SESSION['username'];
  }

  function validateUsername($usr){
    global $usernameReg;
    return (bool) preg_match($usernameReg, $usr);
  }

  function validateEmail($mail){
    global $emailReg;
    return (bool) preg_match($emailReg, $mail);
  }

  function validateLogin($login){
    return validateUsername($login) || validateEmail($login);
  }

  function validateNome($usr){
    global $nomeReg;
    return (bool) preg_match($nomeReg, $usr);
  }

  function validateCognome($mail){
    global $cognomeReg;
    return (bool) preg_match($cognomeReg, $mail);
  }



?>
