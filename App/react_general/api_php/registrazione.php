<?php
require_once "generale.php";

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['nome']) && isset($_POST['cognome']) && isset($_POST['email'])) {
  if(!maybeStartSession()){
    session_start();
  }

  require_once "connessioneDB.php";

  $username = htmlentities(substr($_POST['username'], 0, 64));
  if(debug()){
    $password = htmlentities(substr($_POST['password'], 0, 255));
  }else{
    $password = htmlentities(substr(password_hash($_POST['password'], PASSWORD_DEFAULT), 0, 255));
  }
  $nome = htmlentities(substr($_POST['nome'], 0, 64));
  $cognome = htmlentities(substr($_POST['cognome'], 0, 64));
  $email = substr(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL), 0, 320);


  if(!validateNome($nome) || !validateCognome($cognome) || !validateEmail($email) || !validateUsername($username)){
    sendJsonError("Generico", 'Valori inseriti errati');
  }

  $result = NULL;
  $stmt = $connessione->prepare("SELECT Username FROM utente WHERE Username = ?");

  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $username) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE){
    sendJsonError("Generico", 'Errore query per controllare username');
  }

  if ($result->num_rows >= 1) {
    sendJsonError("Username", "Esiste gia un utente con quell'username");
  }

  $result = NULL;
  $stmt = $connessione->prepare("SELECT Email FROM utente WHERE Email = ?");

  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $email) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE){
      sendJsonError("Generico", 'Errore query per controllare email');
  }

  if ($result->num_rows >= 1) {
    sendJsonError("Email", "Esiste gia un utente con quella email");
  }

  $stmt = $connessione->prepare('INSERT INTO utente(Username, Password, Email, Nome, Cognome) VALUES (?, ?, ?, ?, ?)');
  $result = NULL;
  if(
    $stmt === FALSE ||
    $stmt->bind_param("sssss", $username,  $password, $email, $nome, $cognome) === FALSE ||
    ($result = $stmt->execute()) === FALSE){
      sendJsonError("Generico", 'Errore inserimento nel DB');
  }

  $_SESSION['username'] = $username;
  $_SESSION['permessoUtente'] = 'U';
  sendJsonSuccess([
      'username' => username(),
      'permessoUtente' => permessoUtente(),
      'sessionId' => session_id(),
    ],
    false
  );

}

sendJsonError("Generico", 'Errore parametri');

/*

debug
registrazione.php?username=q&password=q&nome=q&cognome=q&email=q%40q.q

*/
?>
