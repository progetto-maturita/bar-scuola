module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        bar: {
          300: "#FCA5A5",
          500: "#1D4ED8",
          700: "#1D4ED8"
        }
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
