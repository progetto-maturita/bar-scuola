import React from "react";
import { Navbar } from "../components/navbar";
import { Menu } from "../components/menu";

export const MenuPage = () => {
  return (
    <div id="tutto" className="min-flex">
      <Navbar />
      <main id="main" className="min-flex background">
        <Menu />
      </main>
    </div>
  );
};
