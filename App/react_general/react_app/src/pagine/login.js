import React from "react";
import { Navbar } from "../components/navbar";
import { Login } from "../components/login";

export const LoginPage = () => {
  return (
    <div id="tutto" className="min-flex">
      <Navbar />
      <main id="main" className="min-flex background">
        <Login />
      </main>
    </div>
  );
};
