import React from "react";
import { Navbar } from "../components/navbar";

export const IndexPage = () => {
  return (
    <div id="tutto">
      <Navbar />
      <main id="main" className="background"></main>
    </div>
  );
};
