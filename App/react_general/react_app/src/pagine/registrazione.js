import React from "react";
import { Navbar } from "../components/navbar";
import { Registrazione } from "../components/registrazione";

export const RegistrazionePage = () => {
  return (
    <div id="tutto" className="min-flex">
      <Navbar />
      <main id="main" className="min-flex background">
        <Registrazione />
      </main>
    </div>
  );
};
