import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { CostantiContext } from "./context/costantiContext";

//Pagine
import { IndexPage } from "./pagine/index";
import { LoginPage } from "./pagine/login";
import { RegistrazionePage } from "./pagine/registrazione";
import { MenuPage } from "./pagine/menu";

export class App extends Component {
  static contextType = CostantiContext;

  render() {
    return (
      <div
        className={this.context.theme === "dark" ? "dark min-flex" : "min-flex"}
      >
        <div className="background testo min-flex">
          <Router>
            <Switch>
              <Route exact path="/">
                <IndexPage />
              </Route>
              <Route path="/index">
                <IndexPage />
              </Route>
              <Route path="/registrazione">
                <RegistrazionePage />
              </Route>
              <Route path="/login">
                <LoginPage />
              </Route>
              <Route path="/menu">
                <MenuPage />
              </Route>

              <Route>
                <IndexPage />
              </Route>
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}
