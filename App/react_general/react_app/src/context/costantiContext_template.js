import React, { createContext, Component } from "react";
import axios from "axios";
export const CostantiContext = createContext();
export class CostantiContextProvider extends Component {
  state = {
    phpPath: "",
    user: "",
    userPrivileges: "",
    sessionId: "",
    rememberMe: false,
    windowWidth: 0,
    windowHeight: 0,
    theme: "dark"
  };

  updateUser = async utente => {
    if (this.state.user === "") {
      if (this.state.rememberMe) {
        localStorage.setItem("user", JSON.stringify(utente));
      } else {
        sessionStorage.setItem("user", JSON.stringify(utente));
      }
      await this.setState({ user: utente });
    }
  };

  updateUserPrivileges = async permesso => {
    if (this.state.userPrivileges === "") {
      if (this.state.rememberMe) {
        localStorage.setItem("userPrivileges", JSON.stringify(permesso));
      } else {
        sessionStorage.setItem("userPrivileges", JSON.stringify(permesso));
      }
      await this.setState({ userPrivileges: permesso });
    }
  };

  updateRememberMe = async val => {
    if (this.state.user === "" && this.state.userPrivileges === "") {
      await this.setState({ rememberMe: val });
      localStorage.setItem("remember", val);
    }
  };

  updateSessionId = async sess => {
    if (sess !== null && sess !== false) {
      localStorage.setItem("sessionId", JSON.stringify(sess));
      await this.setState({ sessionId: sess });
    }
  };

  getSessionId = async () => {
    if (this.state.sessionId !== null && this.state.sessionId !== "") {
      return this.state.sessionId;
    }
    let sess = JSON.parse(localStorage.getItem("sessionId"));
    await this.setState({ sessionId: sess });
    return sess;
  };

  registrati = async () => {};

  login = async (username, password, rememberMe) => {
    let risultato = "";
    if (username !== "" && password !== "") {
      axios
        .post(
          this.context.phpPath + "login.php",
          {
            username: username,
            password: password,
            sessionId: this.state.sessionId()
          },
          {
            headers: { "content-type": "application/json" }
          }
        )
        .then(response => {
          if (response.data.success === true) {
            this.updateUser(response.data.data.user_nickname);
            this.updateUserPrivileges(response.data.data.permessoUtente);
            this.updateSessionId(response.data.data.sessionId);
            risultato = "Login";
          } else {
            if (
              response.data.data.msg === "Password" ||
              response.data.data.msg === "Utente"
            ) {
              risultato = "Pass-Utente";
            } else if (response.data.data.msg === "Generale") {
              risultato = "Generale";
            }
          }
        });

      return risultato;
    }
  };

  logged = async () => {
    let user = this.state.user,
      userPrivileges = this.state.userPrivileges;

    if (
      user === "undefined" ||
      userPrivileges === "undefined" ||
      user === "" ||
      userPrivileges === "" ||
      user === null ||
      userPrivileges === null
    ) {
      await this.restoreUser();
      user = this.state.user;
      userPrivileges = this.state.userPrivileges;
    }
    if (
      user !== "" &&
      userPrivileges !== "" &&
      user !== null &&
      userPrivileges !== null &&
      user !== "undefined" &&
      userPrivileges !== "undefined"
    ) {
      return true;
    }

    return false;
  };

  logout = () => {
    sessionStorage.removeItem("user");
    sessionStorage.removeItem("userPrivileges");
    sessionStorage.removeItem("sessionId");

    localStorage.removeItem("user");
    localStorage.removeItem("userPrivileges");
    localStorage.removeItem("sessionId");
  };

  restoreUser = async () => {
    let rememberMe = JSON.parse(localStorage.getItem("remember"));
    let user;
    let userPrivileges;
    if (rememberMe) {
      sessionStorage.clear();

      user = JSON.parse(localStorage.getItem("user"));
      userPrivileges = JSON.parse(localStorage.getItem("userPrivileges"));
    } else {
      localStorage.removeItem("user");
      localStorage.removeItem("userPrivileges");
      localStorage.removeItem("sessionId");

      user = JSON.parse(sessionStorage.getItem("user"));
      userPrivileges = JSON.parse(sessionStorage.getItem("userPrivileges"));
    }

    if (
      user !== "" &&
      userPrivileges !== "" &&
      user !== null &&
      userPrivileges !== null &&
      user !== "undefined" &&
      userPrivileges !== "undefined"
    ) {
      await this.setState({ user });
      await this.setState({ userPrivileges });
    }
  };

  clear = () => {
    localStorage.clear();
    sessionStorage.clear();
  };

  updateTheme = async () => {
    let theme = this.state.theme;
    theme = theme === "dark" ? "light" : "dark";
    this.setState({ theme });
    localStorage.setItem("color-theme", JSON.stringify(theme));
  };

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));

    let rememberMe = JSON.parse(localStorage.getItem("remember"));

    if (
      rememberMe !== null &&
      rememberMe !== "undefined" &&
      rememberMe !== ""
    ) {
      this.setState({ rememberMe });
    } else {
      localStorage.setItem("remember", JSON.stringify(this.state.rememberMe));
    }

    const theme = JSON.parse(localStorage.getItem("color-theme"));

    if (theme === null || theme === "undefined" || theme === "") {
      localStorage.setItem("color-theme", JSON.stringify(this.state.theme));
    } else if (theme === "dark" || theme === "light") {
      this.setState({ theme });
    }

    this.restoreUser();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  async updateDimensions() {
    let wW = window.innerWidth;
    let wH = window.innerHeight;
    await this.setState({ windowWidth: wW });
    await this.setState({ windowHeight: wH });
  }

  render() {
    return (
      <CostantiContext.Provider
        value={{
          ...this.state,
          updateUser: this.updateUser,
          updateUserPrivileges: this.updateUserPrivileges,
          updateRememberMe: this.updateRememberMe,
          updateSessionId: this.updateSessionId,
          getSessionId: this.getSessionId,
          registrati: this.registrati,
          login: this.login,
          logged: this.logged,
          logout: this.logout,
          restoreUser: this.restoreUser,
          clear: this.clear,
          updateTheme: this.updateTheme
        }}
      >
        {this.props.children}
      </CostantiContext.Provider>
    );
  }
}
