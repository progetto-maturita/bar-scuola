import React, { Component } from "react";
import { CostantiContext } from "../context/costantiContext";

export class Registrazione extends Component {
  static contextType = CostantiContext;

  state = {
    errore: "",
    nome: "",
    cognome: "",
    email: "",
    username: "",
    password: ""
  };

  async componentDidMount() {
    let logged = await this.context.logged();
    if (logged) {
      window.location.replace("/menu");
      return <div></div>;
    }
  }

  validateNome = nome => {
    const nomeReg = /^(([A-Za-z]+[-']?)*([A-Za-z]+)\s?)+([A-Za-z]+[-']?)*([A-Za-z]+)?$/;
    return nomeReg.test(nome);
  };
  validateCognome = cognome => {
    const cognomeReg = /^(([A-Za-z]+[-']?)*([A-Za-z]+)\s?)+([A-Za-z]+[-']?)*([A-Za-z]+)?$/;
    return cognomeReg.test(cognome);
  };
  validateEmail = email => {
    const emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailReg.test(email);
  };
  validateUser = username => {
    const usernameReg = /^[a-zA-Z0-9!_+\-*#$^.]+$/;
    return usernameReg.test(username);
  };
  validatePassword = password => {
    const strongReg = /^(?=.*[A-Z])(?=.*[-_<>!@#$%^&*.])(?=.*[0-9])(?=.*[a-z]).{8,64}$/;
    const mediumReg = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,64}$/;
    return strongReg.test(password) || mediumReg.test(password);
  };

  registrazione = async () => {
    this.setState({ errore: "" });
    if (
      this.state.nome !== "" &&
      this.state.cognome !== "" &&
      this.state.email !== "" &&
      this.state.username !== "" &&
      this.state.password !== "" &&
      this.validateNome(this.state.nome) &&
      this.validateCognome(this.state.cognome) &&
      this.validateEmail(this.state.email) &&
      this.validateUser(this.state.username) &&
      this.validatePassword(this.state.password)
    ) {
      let err = await this.context.registrati(
        this.state.nome,
        this.state.cognome,
        this.state.email,
        this.state.username,
        this.state.password
      );

      if (err === "OK") {
        window.location.replace("/menu");
        return <div></div>;
      } else {
        this.setState({ errore: err });
      }
    } else {
      let err = "Errori: ";
      if (!this.validateNome(this.state.nome)) {
        err += "\n• nome invalido";
      }
      if (!this.validateCognome(this.state.cognome)) {
        err += "\n• cognome invalido";
      }
      if (!this.validateEmail(this.state.email)) {
        err += "\n• email invalido";
      }
      if (!this.validateUser(this.state.username)) {
        err += "\n• username invalido";
      }
      if (!this.validatePassword(this.state.password)) {
        err += "\n• password invalido";
      }
      this.setState({ errore: err });
    }
  };

  setErrore = async errore => {
    await this.setState({ errore });
  };
  setNome = async nome => {
    await this.setState({ nome });
  };
  setCognome = async cognome => {
    await this.setState({ cognome });
  };
  setEamil = async email => {
    await this.setState({ email });
  };
  setUsername = async username => {
    await this.setState({ username });
  };
  setPassword = async password => {
    await this.setState({ password });
  };

  render() {
    return (
      <div className="min-flex">
        {this.context.windowWidth <= 640 ? (
          <RegistrazioneMobile
            theme={this.context.theme}
            registrazione={this.registrazione}
            errore={this.state.errore}
            setErrore={this.setErrore}
            nome={this.state.nome}
            setNome={this.setNome}
            cognome={this.state.cognome}
            setCognome={this.setCognome}
            email={this.state.email}
            setEamil={this.setEamil}
            username={this.state.username}
            setUsername={this.setUsername}
            password={this.state.password}
            setPassword={this.setPassword}
          />
        ) : (
          <RegistrazioneDesktop
            theme={this.context.theme}
            registrazione={this.registrazione}
            errore={this.state.errore}
            setErrore={this.setErrore}
            nome={this.state.nome}
            setNome={this.setNome}
            cognome={this.state.cognome}
            setCognome={this.setCognome}
            email={this.state.email}
            setEamil={this.setEamil}
            username={this.state.username}
            setUsername={this.setUsername}
            password={this.state.password}
            setPassword={this.setPassword}
          />
        )}
      </div>
    );
  }
}

function RegistrazioneMobile(props) {
  return (
    <div className="w-full flex-grow box-colors rounded-lg mx-auto text-center pt-4 mt-8 rounded-3xl rounded-b-none">
      <h1 className="text-center font-extrabold -mt-3 text-3xl">
        Registrazione
      </h1>
      <div className="container py-5 max-w-md mx-auto">
        <form>
          <div className="mb-4">
            <input
              placeholder="Nome"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.nome}
              onChange={e => {
                props.setNome(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Cognome"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.cognome}
              onChange={e => {
                props.setCognome(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Email"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.email}
              onChange={e => {
                props.setEamil(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Username"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.username}
              onChange={e => {
                props.setUsername(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Password"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              value={props.password}
              onChange={e => {
                props.setPassword(e.target.value);
              }}
            />
          </div>
          <button
            className="box-button-colors font-bold py-3 px-6 text-xl rounded focus:outline-none focus:shadow-outline"
            type="button"
            onClick={e => {
              e.preventDefault();
              props.registrazione();
            }}
          >
            Registrati
          </button>
        </form>
        <div>
          <span>{props.errore !== "" ? props.errore : ""}</span>
        </div>
      </div>
    </div>
  );
}

function RegistrazioneDesktop(props) {
  return (
    <div className="max-w-lg max-w-xs w-5/6 box-colors shadow-2xl rounded-lg mx-auto text-center py-12 mt-4 rounded-xl">
      <h1 className="text-center font-extrabold -mt-3 text-3xl">
        Registrazione
      </h1>
      <div className="container py-5 max-w-md mx-auto">
        <form>
          <div className="mb-4">
            <input
              placeholder="Nome"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.nome}
              onChange={e => {
                props.setNome(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Cognome"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.cognome}
              onChange={e => {
                props.setCognome(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Email"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.email}
              onChange={e => {
                props.setEamil(e.target.value);
              }}
            />
          </div>
          <div className="mb-4">
            <input
              placeholder="Username"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.username}
              onChange={e => {
                props.setUsername(e.target.value);
              }}
            />
          </div>
          <div className="mb-6">
            <input
              placeholder="Password"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              value={props.password}
              onChange={e => {
                props.setPassword(e.target.value);
              }}
            />
          </div>

          <button
            className="box-button-colors font-bold py-2 px-4 text-xl rounded focus:outline-none focus:shadow-outline"
            type="button"
            onClick={e => {
              e.preventDefault();
              props.registrazione();
            }}
          >
            Registrati
          </button>
        </form>
        <div>
          <span>{props.errore !== "" ? props.errore : ""}</span>
        </div>
      </div>
    </div>
  );
}
