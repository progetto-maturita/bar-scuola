import React, { Component } from "react";
import { Link } from "react-router-dom";
import { CostantiContext } from "../context/costantiContext";

export class Login extends Component {
  static contextType = CostantiContext;

  state = {
    errore: "",
    username: "",
    password: "",
    rememberMe: false
  };

  async componentDidMount() {
    let logged = await this.context.logged();
    if (logged) {
      window.location.replace("/menu");
      return <div></div>;
    }
  }

  validateUserEmail = usernameEmail => {
    const user = /^[a-zA-Z0-9!_+\-*#$^.]+$/;
    const email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return user.test(usernameEmail) || email.test(usernameEmail);
  };

  validatePassword = password => {
    const strongReg = /^(?=.*[A-Z])(?=.*[-_<>!@#$%^&*.])(?=.*[0-9])(?=.*[a-z]).{8,64}$/;
    const mediumReg = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,64}$/;
    return strongReg.test(password) || mediumReg.test(password);
  };

  login = async () => {
    this.setState({ errore: "" });
    if (
      this.state.username !== "" &&
      this.state.password !== "" &&
      (this.state.rememberMe === true || this.state.rememberMe === false) &&
      this.validateUserEmail(this.state.username) &&
      this.validatePassword(this.state.password)
    ) {
      const err = await this.context.login(
        this.state.username,
        this.state.password,
        this.state.rememberMe
      );

      if (err === "OK") {
        window.location.replace("/menu");
        return <div></div>;
      } else {
        this.setState({ errore: err });
      }
    } else {
      let err = "Errori: ";
      if (!this.validateUserEmail(this.state.username)) {
        err += "\n• username invalido";
      }
      if (!this.validatePassword(this.state.password)) {
        err +=
          "\n• password invalida\n Deve essere almeno lugna 6 caratteri, avere una lettara minuscola, maiuscola e un numero";
      }
      this.setState({ errore: err });
    }
  };

  setErrore = async errore => {
    await this.setState({ errore });
  };
  setUsername = async username => {
    await this.setState({ username });
  };
  setPassword = async password => {
    await this.setState({ password });
  };
  setRememberMe = async rememberMe => {
    await this.setState({ rememberMe });
  };

  render() {
    return (
      <div className="min-flex">
        {this.context.windowWidth <= 640 ? (
          <LoginMobile
            theme={this.context.theme}
            login={this.login}
            errore={this.state.errore}
            setErrore={this.setErrore}
            username={this.state.username}
            setUsername={this.setUsername}
            password={this.state.password}
            setPassword={this.setPassword}
            rememberMe={this.state.rememberMe}
            setRememberMe={this.setRememberMe}
          />
        ) : (
          <LoginDesktop
            theme={this.context.theme}
            login={this.login}
            errore={this.state.errore}
            setErrore={this.setErrore}
            username={this.state.username}
            setUsername={this.setUsername}
            password={this.state.password}
            setPassword={this.setPassword}
            rememberMe={this.state.rememberMe}
            setRememberMe={this.setRememberMe}
          />
        )}
      </div>
    );
  }
}

function LoginMobile(props) {
  return (
    <div className="w-full flex-grow box-colors rounded-lg mx-auto text-center pt-4 mt-8 rounded-3xl rounded-b-none">
      <h1 className="text-center font-extrabold -mt-3 text-3xl">Login</h1>
      <div className="container py-5 max-w-md mx-auto">
        <form>
          <div className="mb-4">
            <input
              placeholder="Username/Email"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.username}
              onChange={e => {
                props.setUsername(e.target.value);
              }}
            />
          </div>
          <div className="">
            <input
              placeholder="Password"
              className="shadow appearance-none text-black rounded w-11/12 py-2 px-3 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              value={props.password}
              onChange={e => {
                props.setPassword(e.target.value);
              }}
            />
          </div>
          <div>
            <label className="inline-flex cursor-pointer">
              <input
                type="checkbox"
                className="form-checkbox w-5 h-5"
                onChange={e => {
                  props.setRememberMe(e.target.checked);
                }}
              />
              <span className="ml-2 text-sm font-semibold">Remember me</span>
            </label>
          </div>
          <div className="flex flex-col items-center justify-between pt-3">
            <button
              className="box-button-colors font-bold py-3 px-6 text-xl rounded focus:outline-none focus:shadow-outline"
              type="button"
              onClick={e => {
                e.preventDefault();
                props.login();
              }}
            >
              Log in
            </button>
            <Link
              className="inline-block align-baseline font-bold text-sm pt-3"
              to="/recupera-password"
            >
              Password dimenticata?
            </Link>
          </div>
        </form>
        <div>
          <span>{props.errore !== "" ? props.errore : ""}</span>
        </div>
      </div>
    </div>
  );
}

function LoginDesktop(props) {
  return (
    <div className="max-w-lg max-w-xs w-5/6 box-colors shadow-2xl rounded-lg mx-auto text-center py-12 mt-4 rounded-xl">
      <h1 className="text-center font-extrabold -mt-3 text-3xl">Login</h1>
      <div className="container py-5 max-w-md mx-auto">
        <form>
          <div className="mb-4">
            <input
              placeholder="Username/Email"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              value={props.username}
              onChange={e => {
                props.setUsername(e.target.value);
              }}
            />
          </div>
          <div className="mb-6">
            <input
              placeholder="Password"
              className="shadow appearance-none text-black rounded w-full py-2 px-3 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              value={props.password}
              onChange={e => {
                props.setPassword(e.target.value);
              }}
            />
          </div>
          <div>
            <label className="inline-flex cursor-pointer">
              <input
                type="checkbox"
                className="form-checkbox w-5 h-5"
                onChange={e => {
                  props.setRememberMe(e.target.checked);
                }}
              />
              <span className="ml-2 text-sm font-semibold">Remember me</span>
            </label>
          </div>
          <div className="flex items-center justify-between">
            <button
              className="box-button-colors font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="button"
              onClick={e => {
                e.preventDefault();
                props.login();
              }}
            >
              Log in
            </button>
            <Link
              className="inline-block align-baseline font-bold text-sm "
              to="/recupera-password"
            >
              Password dimenticata?
            </Link>
          </div>
        </form>
        <div>
          <span>{props.errore !== "" ? props.errore : ""}</span>
        </div>
      </div>
    </div>
  );
}
