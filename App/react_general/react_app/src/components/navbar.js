import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { CostantiContext } from "../context/costantiContext";
import { ThemeToggle } from "./themeToggle";
import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { bubble as MenuMobile } from "react-burger-menu";

export class Navbar extends Component {
  static contextType = CostantiContext;

  render() {
    return (
      <div>
        {this.context.windowWidth <= 640 ? (
          <NavMobile
            theme={this.context.theme}
            user={this.context.user}
            logout={this.context.logout}
          />
        ) : (
          <NavDesktop
            theme={this.context.theme}
            user={this.context.user}
            logout={this.context.logout}
          />
        )}
      </div>
    );
  }
}

function NavMobile(props) {
  var styles = {
    bmBurgerButton: {
      position: "absolute",
      width: "30px",
      height: "26px",
      left: "18px",
      top: "18px"
    },
    bmBurgerBars: {
      background: "#000000"
    },
    bmBurgerBarsHover: {
      background: "#a90000"
    },
    bmCrossButton: {
      height: "30px",
      width: "30px"
    },
    bmCross: {
      background: "#bdc3c7"
    },
    bmMenuWrap: {
      position: "fixed",
      height: "100%"
    },
    bmOverlay: {
      background: "rgba(0, 0, 0, 0.3)"
    }
  };
  return (
    <header className="navbar-colors">
      <nav className="flex text-center flex-row justify-between pb-4 pr-6 navbar-colors w-full">
        <div className="mb-2 sm:mb-0">
          {props.user !== "" ? (
            <MenuMobile
              styles={styles}
              pageWrapId={"main"}
              outerContainerId={"tutto"}
            >
              <h2>Bar Severi</h2>
              <Link to="/menu" className="navbar-desktop-link pt-2">
                <div className="flex flex-row justify-center">
                  <svg className="h-6 w-6 fill-current" viewBox="0 0 24 24">
                    <path
                      fillRule="evenodd"
                      fill={props.theme === "dark" ? "#FFF" : "#000"}
                      d="M8.1 13.34l2.83-2.83L3.91 3.5c-1.56 1.56-1.56 4.09 0 5.66l4.19 4.18zm6.78-1.81c1.53.71 3.68.21 5.27-1.38 1.91-1.91 2.28-4.65.81-6.12-1.46-1.46-4.2-1.1-6.12.81-1.59 1.59-2.09 3.74-1.38 5.27L3.7 19.87l1.41 1.41L12 14.41l6.88 6.88 1.41-1.41L13.41 13l1.47-1.47z"
                    />
                  </svg>
                  <span>Menu</span>
                </div>
              </Link>
              <Link to="/ordini" className="navbar-desktop-link pt-2">
                <div className="flex flex-row justify-center">
                  <svg
                    className="w-6 h-6"
                    viewBox="0 0 24 24"
                    fill={props.theme === "dark" ? "#FFF" : "#000"}
                  >
                    <g>
                      <rect fill="none" height="24" width="24" />
                    </g>
                    <g>
                      <g>
                        <path
                          d="M22,10c0.32-3.28-4.28-6-9.99-6C6.3,4,1.7,6.72,2.02,10H22z"
                          fillRule="evenodd"
                        />
                        <path
                          d="M5.35,13.5c0.55,0,0.78,0.14,1.15,0.36c0.45,0.27,1.07,0.64,2.18,0.64 s1.73-0.37,2.18-0.64c0.37-0.23,0.59-0.36,1.15-0.36c0.55,0,0.78,0.14,1.15,0.36c0.45,0.27,1.07,0.64,2.18,0.64 c1.11,0,1.73-0.37,2.18-0.64c0.37-0.23,0.59-0.36,1.15-0.36c0.55,0,0.78,0.14,1.15,0.36c0.45,0.27,1.07,0.63,2.17,0.64v-1.98 c0,0-0.79-0.16-1.16-0.38c-0.45-0.27-1.07-0.64-2.18-0.64c-1.11,0-1.73,0.37-2.18,0.64c-0.37,0.23-0.6,0.36-1.15,0.36 s-0.78-0.14-1.15-0.36c-0.45-0.27-1.07-0.64-2.18-0.64s-1.73,0.37-2.18,0.64c-0.37,0.23-0.59,0.36-1.15,0.36 c-0.55,0-0.78-0.14-1.15-0.36c-0.45-0.27-1.07-0.64-2.18-0.64c-1.11,0-1.73,0.37-2.18,0.64C2.78,12.37,2.56,12.5,2,12.5v2 c1.11,0,1.73-0.37,2.21-0.64C4.58,13.63,4.8,13.5,5.35,13.5z"
                          fillRule="evenodd"
                        />
                        <path
                          d="M2,16v2c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2v-2H2z"
                          fillRule="evenodd"
                        />
                      </g>
                    </g>
                  </svg>
                  <span>Ordini</span>
                </div>
              </Link>
              <Link to="/portafoglio" className="navbar-desktop-link pt-4">
                <div className="flex flex-row justify-center">
                  <svg
                    className="w-6 h-6"
                    viewBox="0 0 24 24"
                    fill={props.theme === "dark" ? "#FFF" : "#000"}
                  >
                    <path d="M21 18v1c0 1.1-.9 2-2 2H5c-1.11 0-2-.9-2-2V5c0-1.1.89-2 2-2h14c1.1 0 2 .9 2 2v1h-9c-1.11 0-2 .9-2 2v8c0 1.1.89 2 2 2h9zm-9-2h10V8H12v8zm4-2.5c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5z" />
                  </svg>
                  <span>Portafoglio</span>
                </div>
              </Link>
              <Link to="/utente" className="navbar-desktop-link pt-4">
                <div className="flex flex-row justify-center">
                  <svg
                    className="w-6 h-6"
                    viewBox="0 0 24 24"
                    fill={props.theme === "dark" ? "#FFF" : "#000"}
                  >
                    <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
                  </svg>{" "}
                  <span>Utente</span>
                </div>
              </Link>
              <Link to="/" className="navbar-desktop-link pt-4">
                <div className="flex flex-row justify-center">
                  <button
                    aria-label="logout"
                    className="font-semibold"
                    onClick={() => {
                      props.logout();
                    }}
                  >
                    <svg
                      className="w-6 h-6"
                      viewBox="0 0 24 24"
                      fill={props.theme === "dark" ? "#FFF" : "#000"}
                    >
                      <path d="M0 0h24v24H0z" fill="none" />
                      <path d="M17 7l-1.41 1.41L18.17 11H8v2h10.17l-2.58 2.58L17 17l5-5zM4 5h8V3H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h8v-2H4V5z" />
                    </svg>
                    <span>Logout</span>
                  </button>
                </div>
              </Link>
              <div className="navbar-desktop-link pt-4 flex flex-row justify-center">
                <ThemeToggle />
              </div>
            </MenuMobile>
          ) : (
            <MenuMobile
              styles={styles}
              pageWrapId={"main"}
              outerContainerId={"tutto"}
            >
              <h2>Bar Severi</h2>
              <Link to="/menu" className="navbar-desktop-link">
                <span>Menu</span>
              </Link>
              <Link to="/login" className="navbar-desktop-link">
                <span>Login</span>
              </Link>
              <Link to="/registrazione" className="navbar-desktop-link">
                <span>Resgistrati</span>
              </Link>
            </MenuMobile>
          )}
        </div>
        <div className="flex flex-row pt-4">
          {props.user !== "" ? (
            <Link to="/carrello" className="navbar-desktop-link">
              <svg
                className="w-6 h-6"
                viewBox="0 0 24 24"
                fill={props.theme === "dark" ? "#FFF" : "#000"}
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z" />
              </svg>
            </Link>
          ) : (
            <ThemeToggle />
          )}
        </div>
      </nav>
    </header>
  );
}

function NavDesktop(props) {
  return (
    <header className="navbar-colors">
      <nav className="flex text-center flex-row justify-between py-4 px-6 navbar-colors w-full">
        <div className="mb-2 sm:mb-0">
          <Link to="/" className="navbar-desktop-link">
            <span>Bar Severi</span>
          </Link>
        </div>
        <div>
          <Link to="/menu" className="navbar-desktop-link pr-5">
            <span>Menu</span>
          </Link>
        </div>
        <div className="flex flex-row">
          {props.user !== "" ? (
            <div className="flex flex-row">
              <Dropdown
                theme={props.theme}
                user={props.user}
                logout={props.logout}
              />
              <Link to="/carrello" className="navbar-desktop-link pr-2">
                <div className="flex flex-row justify-center items-center">
                  <svg
                    className="w-5 h-5"
                    viewBox="0 0 24 24"
                    fill={props.theme === "dark" ? "#FFF" : "#000"}
                  >
                    <path d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z" />
                  </svg>
                  <span>Carrello</span>
                </div>
              </Link>
            </div>
          ) : (
            <div>
              <Link to="/login" className="navbar-desktop-link pr-5">
                <span>Login</span>
              </Link>
              <Link to="/registrazione" className="navbar-desktop-link pr-5">
                <span>Resgistrati</span>
              </Link>
            </div>
          )}
          <ThemeToggle />
        </div>
      </nav>
    </header>
  );
}

function Dropdown(props) {
  const user = props.user[0].toUpperCase() + props.user.slice(1);

  return (
    <Menu as="div" className="relative inline-block text-left pr-2">
      {({ open }) => (
        <>
          <div>
            <Menu.Button className="inline-flex justify-center w-full focus:outline-none navbar-color pr-2 font-semibold text-xl">
              {user}
              <ChevronDownIcon
                className="mt-1 ml-2 h-6 w-6 font-semibold text-xl"
                aria-hidden="true"
              />
            </Menu.Button>
          </div>

          <Transition
            show={open}
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items
              static
              className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg navbar-desktop-dropdown-colors ring-1 ring-black ring-opacity-5 focus:outline-none"
            >
              <div className="py-1">
                <Menu.Item>
                  {({ active }) => (
                    <Link to="/ordini" className="navbar-desktop-link pt-2">
                      <div className="flex flex-row justify-center">
                        <svg
                          className="w-6 h-6"
                          viewBox="0 0 24 24"
                          fill={props.theme === "dark" ? "#FFF" : "#000"}
                        >
                          <g>
                            <rect fill="none" height="24" width="24" />
                          </g>
                          <g>
                            <g>
                              <path
                                d="M22,10c0.32-3.28-4.28-6-9.99-6C6.3,4,1.7,6.72,2.02,10H22z"
                                fillRule="evenodd"
                              />
                              <path
                                d="M5.35,13.5c0.55,0,0.78,0.14,1.15,0.36c0.45,0.27,1.07,0.64,2.18,0.64 s1.73-0.37,2.18-0.64c0.37-0.23,0.59-0.36,1.15-0.36c0.55,0,0.78,0.14,1.15,0.36c0.45,0.27,1.07,0.64,2.18,0.64 c1.11,0,1.73-0.37,2.18-0.64c0.37-0.23,0.59-0.36,1.15-0.36c0.55,0,0.78,0.14,1.15,0.36c0.45,0.27,1.07,0.63,2.17,0.64v-1.98 c0,0-0.79-0.16-1.16-0.38c-0.45-0.27-1.07-0.64-2.18-0.64c-1.11,0-1.73,0.37-2.18,0.64c-0.37,0.23-0.6,0.36-1.15,0.36 s-0.78-0.14-1.15-0.36c-0.45-0.27-1.07-0.64-2.18-0.64s-1.73,0.37-2.18,0.64c-0.37,0.23-0.59,0.36-1.15,0.36 c-0.55,0-0.78-0.14-1.15-0.36c-0.45-0.27-1.07-0.64-2.18-0.64c-1.11,0-1.73,0.37-2.18,0.64C2.78,12.37,2.56,12.5,2,12.5v2 c1.11,0,1.73-0.37,2.21-0.64C4.58,13.63,4.8,13.5,5.35,13.5z"
                                fillRule="evenodd"
                              />
                              <path
                                d="M2,16v2c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2v-2H2z"
                                fillRule="evenodd"
                              />
                            </g>
                          </g>
                        </svg>
                        Ordini
                      </div>
                    </Link>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <Link
                      to="/portafoglio"
                      className="navbar-desktop-link pt-2"
                    >
                      <div className="flex flex-row justify-center">
                        <svg
                          className="w-6 h-6"
                          viewBox="0 0 24 24"
                          fill={props.theme === "dark" ? "#FFF" : "#000"}
                        >
                          <path d="M21 18v1c0 1.1-.9 2-2 2H5c-1.11 0-2-.9-2-2V5c0-1.1.89-2 2-2h14c1.1 0 2 .9 2 2v1h-9c-1.11 0-2 .9-2 2v8c0 1.1.89 2 2 2h9zm-9-2h10V8H12v8zm4-2.5c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5z" />
                        </svg>
                        Portafoglio
                      </div>
                    </Link>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <Link to="/utente" className="navbar-desktop-link pt-2">
                      <div className="flex flex-row justify-center">
                        <svg
                          className="w-6 h-6"
                          viewBox="0 0 24 24"
                          fill={props.theme === "dark" ? "#FFF" : "#000"}
                        >
                          <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
                        </svg>{" "}
                        Utente
                      </div>
                    </Link>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <Link to="/" className="navbar-desktop-link pt-2">
                      <div className="flex flex-row justify-center">
                        <button
                          aria-label="logout"
                          className="font-semibold flex flex-row justify-center"
                          onClick={() => {
                            props.logout();
                          }}
                        >
                          <svg
                            className="w-6 h-6"
                            viewBox="0 0 24 24"
                            fill={props.theme === "dark" ? "#FFF" : "#000"}
                          >
                            <path d="M0 0h24v24H0z" fill="none" />
                            <path d="M17 7l-1.41 1.41L18.17 11H8v2h10.17l-2.58 2.58L17 17l5-5zM4 5h8V3H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h8v-2H4V5z" />
                          </svg>
                          Logout
                        </button>
                      </div>
                    </Link>
                  )}
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </>
      )}
    </Menu>
  );
}
