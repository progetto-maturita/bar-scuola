import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { App } from "./App";
import * as serviceWorkerRegistration from "./serviceWorkerRegistration";
import reportWebVitals from "./reportWebVitals";
import { CostantiContextProvider } from "./context/costantiContext";

ReactDOM.render(
  <React.StrictMode>
    <CostantiContextProvider>
      <App />
    </CostantiContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

serviceWorkerRegistration.unregister();
