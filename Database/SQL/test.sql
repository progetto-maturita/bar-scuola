USE Bar;

INSERT INTO utente (Username, Password, Email, Nome, Cognome, Permesso) VALUES ("u", "u", "u@u.u", "u", "u", 'U'), ("b", "b", "b@b.b", "b", "b", 'B'), ("s", "s", "s@s.s", "s", "s", 'S'), ("t", "t", "t@t.t", "t", "t", 'T');

INSERT INTO classe (Nome) VALUES ("1EA"), ("1IA"), ("1IB"), ("1IC"), ("1ID"), ("1IE"), ("1IF"), ("1IG"), ("1MA"), ("2EA"), ("2EB"), ("2IA"), ("2IB"), ("2IC"), ("2ID"), ("2IE"), ("2IF"), ("2IG"), ("2IH"), ("2MA"), ("2MB"), ("3IA"), ("3IB"), ("3IC"), ("3ID"), ("3IE"), ("3MA"), ("3MB"), ("3MC"), ("3UA"), ("3UB"), ("4EA"), ("4IA/MC"), ("4IB"), ("4IC"), ("4ID"), ("4IE"), ("4IF"), ("4MA"), ("4MB"), ("5EA"), ("5IA"), ("5IB"), ("5IC/MB"), ("5ID"), ("5IE"), ("5MA"), ("5UA");

INSERT INTO utente (Username, Password, Email, Nome, Cognome, Permesso, Classe) VALUES ("QDX49YII3FV","o","ultrices@magnaaneque.com","Graiden","Gregory","S","1EA"),("QRB13GUO5JU","o","vestibulum@Donec.org","Norman","Wolf","S","1EA"),("KWP45IFK7WP","o","est@nullamagna.net","Steven","Hill","S","1EA"),("EHC51AMZ6VO","o","ut@placerat.co.uk","Reece","Gallagher","S","1EA"),("LYJ46NBO2XH","o","nunc.sed.pede@acfacilisisfacilisis.com","Odysseus","Todd","S","1EA"),("GJD74AHD6RT","o","sit@Donec.org","Bradley","King","S","1EA"),("IMV76KNY0NZ","o","Aenean.eget@arcuVestibulum.co.uk","Clayton","Simmons","S","1EA"),("AJM48DHD5DJ","o","urna.Vivamus@aliquet.org","Dorian","Knox","S","1EA"),("YYT50ILJ8AB","o","quis@loremipsum.org","Martin","Sanchez","S","1EA"),("WLZ55MFK0IQ","o","imperdiet.erat.nonummy@mollis.co.uk","Francis","Maynard","S","1EA"),("VSS57WJG4PB","o","erat@ornarelectus.com","Demetrius","Jacobson","S","1EA"),("YUS71FNL0IM","o","non.quam.Pellentesque@placeratorcilacus.net","Ronan","Charles","S","1EA"),("ZXV28WHA6JS","o","blandit.at@enim.org","Evan","Case","S","1EA"),("HXM96BWC3RX","o","ac.tellus.Suspendisse@lorem.com","Tiger","Mcintosh","S","1EA"),("PTW10BLS9DX","o","sed@nectempus.com","Kirk","Mosley","S","1EA"),("XJO91GVY0XJ","o","Cras@pharetrasedhendrerit.org","Erich","Mcclure","S","1EA"),("OLC57VYJ2OH","o","ridiculus.mus@ac.org","Yasir","Boyd","S","1EA"),("LGO99MNL8HA","o","at.egestas.a@fermentum.com","Byron","Dalton","S","1EA"),("SAZ39JFM0SY","o","orci.sem@dolorsit.com","Ferdinand","Foster","S","1EA"),("FTY89EZM9UG","o","orci.Ut@dolor.net","Robert","Bryan","S","1EA");

INSERT INTO rappresentanteClasse (Rappresentante, Classe) VALUES ("QDX49YII3FV","1EA"), ("QRB13GUO5JU","1EA");

INSERT into luogoRitiro (Luogo) VALUES ("Bar centrale"), ("Ampliamento"), ("Palestra");

ALTER TABLE categoria AUTO_INCREMENT = 1;

INSERT INTO categoria VALUES ("Bibite", "Merenda", "Bibite da bere"), ("Panini", "Merenda", "Panini"), ("Insalata", "Pranzo", "Insalata");

ALTER TABLE famiglia AUTO_INCREMENT = 1;

INSERT INTO famiglia (Descrizione, Categoria) VALUES ("Acqua", "Bibite"), ("Bibite gassate", "Bibite"), ("Panino con cotoletta", "Panini"), ("Panino con prociutto cotto", "Panini"),  ("Panino con pancetta", "Panini"), ("Panino con mortadella", "Panini"), ("Insalata con pollo", "Insalata");

ALTER TABLE componente AUTO_INCREMENT = 1;

INSERT INTO componente (Descrizione, Prezzo) VALUES ("Pane", 50), ("Salame", 50), ("Cotoletta", 70), ("Prosciutto cotto", 60), ("Prosciutto crudo", 70), ("Acqua Naturale", 100), ("Acqua Frizzante", 100), ("Pepsi", 110), ("Coca cola", 110), ("Maionese", 10);

INSERT INTO prodotto (ID, IDFamiglia, Descrizione) VALUES  (0, 1, "Acqua Naturale"), (0, 2, "Coca Cola"), (1, 1, "Acqua Frizzante"), (1, 2, "Pepsi");

INSERT INTO componenteProdotto VALUES (1, 0, 6, 1), (2, 1, 8, 1), (2, 0, 9, 1), (1, 0, 7, 1);

INSERT INTO prodotto (ID, IDFamiglia, Descrizione) VALUES  (0, 3, "Panino con cotoletta"), (1, 3, "Panino con cotoletta senza maionese");

INSERT INTO componenteProdotto VALUES (3, 0, 1, 1), (3, 0, 3, 1), (3, 0, 10, 1);
INSERT INTO componenteProdotto VALUES (3, 1, 1, 1), (3, 1, 3, 1);
