/*

TODO:
  utente:
    x OTP
    -----------------------
  classe:
    -----------------------
  rappresentanteClasse
    -----------------------
  ricarica
    -----------------------
  ordine:
    -----------------------
   transazione
    -----------------------
  categoria:
    -----------------------
  famiglia:
    -----------------------
  prodotto
    -----------------------
  prodottoOrdine
    -----------------------
  componenteProdotto
    -----------------------
*/

CREATE DATABASE IF NOT EXISTS Bar;

USE Bar;

CREATE TABLE IF NOT EXISTS classe (
  Nome CHAR(6) NOT NULL, /*Nome della classe*/
  PRIMARY KEY (Nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS utente (
  Username VARCHAR(64) NOT NULL, /*Username dell'utente*/
  Password VARCHAR(255) NOT NULL, /*255 caratteri sono consigliati se usi PASSWORD_DEFAULT fonte: https://www.php.net/manual/en/function.password-hash.php/ */
  Email VARCHAR(320) NOT NULL UNIQUE, /*320 caratteri perche' l'username all'interno del server puo' essere lungo massimo  caratteri, il simbolo @, poi il dominio può essere lungo massimo 255 caratteri*/
  Nome VARCHAR(64) NOT NULL, /*Nome dell'utente*/
  Cognome VARCHAR(64) NOT NULL,/*Cognome dell'utente*/
  Credito SMALLINT UNSIGNED NOT NULL DEFAULT 0, /*Credito espresso in centesimi da 0 a 65535 => massimo 655,35 euro sul bilancio*/
  Permesso ENUM("U", "S", "B", "T") NOT NULL DEFAULT "U", /*Il permesso che possiede un determinato utente.
  U: Utente -- Un utente normale ha il permesso solo di creare ordini e pagare con l'app
  S: Studente -- Uno studente ha i permessi di un utente e in piu ha i privilegi di fare un ordine e farlo ritirare al rappresentante di classe
  B: Barista -- Un barista ha il permesso di visualizzare gli ordini e di caricare credito
  T: Totem -- Un totem ha il permesso di caricare credito
  */
  Classe CHAR(6), /*Serve nel caso un utente sia uno studente per sapere che rappresentante deve prendere i suoi ordini*/
  /*OTP Da implementare => Serve per implementare l'autenticazione a due fattori*/
  PRIMARY KEY (Username),
  FOREIGN KEY (Classe) REFERENCES classe(Nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS rappresentanteClasse(
  Rappresentante VARCHAR(64) NOT NULL, /*Username del rappresentante*/
  Classe CHAR(6) NOT NULL, /*Classe del rappresentante*/
  PRIMARY KEY (Rappresentante, Classe),
  FOREIGN KEY (Classe) REFERENCES classe(Nome),
  FOREIGN KEY (Rappresentante) REFERENCES utente(Username)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS ricarica (
  ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, /*ID univoco di una ricarica*/
  OrarioRicarica DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, /*Orario a cui e' stata creata la ricarica*/
  UtenteRicaricato VARCHAR(64) NOT NULL, /*Utente che ha ricevuto la ricarica*/
  UtenteCaricatore VARCHAR(64) NOT NULL, /*Utente che ha effettuato la ricarica*/
  PRIMARY KEY (ID),
  FOREIGN KEY (UtenteRicaricato) REFERENCES utente(Username),
  FOREIGN KEY (UtenteCaricatore) REFERENCES utente(Username)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS luogoRitiro (
  ID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, /*ID univoco di un luogo per ritirare la merenda*/
  Luogo VARCHAR(512) NOT NULL, /*Descrizione del luogo per ritirare*/
  PRIMARY KEY (ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS ordine (
  ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, /*ID univoco di un ordine*/
  LuogoRitiro SMALLINT UNSIGNED NOT NULL, /*ID di un luogo di ritiro*/
  OrarioRitiro DATETIME, /*Orario e giorno del ritiro*/
  Ritirato BOOLEAN NOT NULL DEFAULT FALSE, /*Indica se l'ordine e' gia stato ordinato*/
  Annullato BOOLEAN NOT NULL DEFAULT FALSE, /*Indica se e' stato annullato l'ordine*/
  Rappresentante BOOLEAN NOT NULL DEFAULT FALSE, /*Indica se l'ordine sara ritirato da un rappresentante*/
  Note VARCHAR(512), /*Note opzionali per l'ordine*/
  Utente VARCHAR(64) NOT NULL, /*Utente che ha creato l'ordine*/
  PRIMARY KEY (ID),
  FOREIGN KEY (LuogoRitiro) REFERENCES luogoRitiro(ID),
  FOREIGN KEY (Utente) REFERENCES utente(Username)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS transazione (
  ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, /*ID univoco di una transazione*/
  IDOrdine BIGINT UNSIGNED NOT NULL, /*ID univoco che si riferisce a un ordine*/
  OrarioCreazione DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, /*Orario e giorno della creazione dell'ordine*/
  OrarioUltimaModifica DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, /*Orario e giorno dell'ultima modifica dell'ordine*/
  Annullato BOOLEAN NOT NULL DEFAULT FALSE, /*Indica se e' stato annullato l'ordine*/
  Pagato BOOLEAN NOT NULL DEFAULT FALSE, /*Indica se e' stato pagato*/
  Prezzo SMALLINT UNSIGNED NOT NULL, /*Prezzo da pagare*/
  Note VARCHAR(1024), /*Note pzionali per la transazione*/
  PRIMARY KEY (ID),
  FOREIGN KEY (IDOrdine) REFERENCES ordine(ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS categoria (
  Nome VARCHAR(64) NOT NULL, /*Nome della categoria*/
  Pasto VARCHAR(64) NOT NULL, /*Pasto a cui appertiene la categoria*/
  Descrizione VARCHAR(512) NOT NULL, /*Piccola descrizione della categoria*/
  PRIMARY KEY (Nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS famiglia (
  ID INT UNSIGNED NOT NULL AUTO_INCREMENT, /*ID univoco di una famiglia di prodotti*/
  Descrizione VARCHAR(512) NOT NULL, /*Piccola descrizione della famiglia*/
  Categoria VARCHAR(64) NOT NULL, /*Categoria a cui appartiene la famiglia*/
  PRIMARY KEY (ID),
  FOREIGN KEY (Categoria) REFERENCES categoria(Nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS prodotto (
  ID INT UNSIGNED NOT NULL, /*ID del prodotto. Viene usata una convenzione dove :
  ID == 0 => Versione principale del prodotto
  ID != 0 => Varianti del prodotto*/
  IDFamiglia INT UNSIGNED NOT NULL, /*ID della famiglia a cui appartiene il prodotto*/
  Descrizione VARCHAR(512) NOT NULL, /*Descrizione del prodotto*/
  PRIMARY KEY (ID, IDFamiglia),
  FOREIGN KEY (IDFamiglia) REFERENCES famiglia(ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS prodottoOrdine (
  IDOrdine BIGINT UNSIGNED NOT NULL, /*ID dell'ordine*/
  IDFamiglia INT UNSIGNED NOT NULL, /*ID della famiglia del prodotto ordinato*/
  IDProdotto INT UNSIGNED NOT NULL, /*ID del prodotto ordinato*/
  Quantita SMALLINT UNSIGNED NOT NULL DEFAULT 1, /*Quantita ordinata*/
  PRIMARY KEY (IDOrdine, IDProdotto, IDFamiglia),
  FOREIGN KEY (IDOrdine) REFERENCES ordine(ID),
  FOREIGN KEY (IDFamiglia) REFERENCES prodotto(IDFamiglia),
  FOREIGN KEY (IDProdotto) REFERENCES prodotto(ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS componente (
  ID INT UNSIGNED NOT NULL AUTO_INCREMENT, /*ID del componente*/
  Descrizione VARCHAR(512) NOT NULL, /*Descrizione del componente*/
  Prezzo INT UNSIGNED NOT NULL, /*Prezzo in centesimi di euro*/
  PRIMARY KEY (ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS componenteProdotto (
  IDFamiglia INT UNSIGNED NOT NULL, /*ID della famiglia a cui appartiene il prodotto*/
  IDProdotto INT UNSIGNED NOT NULL, /*ID del prodotto*/
  IDComponente INT UNSIGNED NOT NULL, /*ID del compnente all'interno del prodotto*/
  Quantita SMALLINT UNSIGNED NOT NULL DEFAULT 1, /*Quantita del componente all'interno del prodotto*/
  PRIMARY KEY (IDFamiglia, IDProdotto, IDComponente),
  FOREIGN KEY (IDFamiglia) REFERENCES prodotto(IDFamiglia),
  FOREIGN KEY (IDProdotto) REFERENCES prodotto(ID),
  FOREIGN KEY (IDComponente) REFERENCES componente(ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;
