/*Query che ritorna il totale ricaricato e pagato di ogni utente*/
SELECT transazioniOrdini.Utente, ricarica.TotaleRicaricato, transazioniOrdini.TotalePagato FROM (SELECT UtenteRicaricato, SUM(Importo) AS TotaleRicaricato FROM ricarica GROUP BY UtenteRicaricato) as ricarica INNER JOIN (SELECT SUM(Prezzo) AS TotalePagato, ordine.Utente FROM transazione INNER JOIN ordine ON transazione.IDOrdine = ordine.ID GROUP BY Utente) AS transazioniOrdini ON ricarica.UtenteRicaricato = transazioniOrdini.Utente;

/*Query che ritorna il luogo di ritiro piu utilizzato*/
SELECT luogoritiro.Luogo, TotaleRitiri  FROM (SELECT COUNT(*) AS "TotaleRitiri", LuogoRitiro FROM ordine GROUP BY LuogoRitiro) AS conteggio INNER JOIN luogoritiro ON luogoritiro.ID = conteggio.LuogoRitiro ORDER BY TotaleRitiri DESC LIMIT 1;

/*Query che ritorna il luogo di ritiro piu utilizzato in un certo periodo*/
SELECT luogoritiro.Luogo, TotaleRitiri  FROM (SELECT COUNT(*) AS "TotaleRitiri", LuogoRitiro FROM ordine WHERE OrarioRitiro BETWEEN '2021-01-03' AND '2021-05-23' GROUP BY LuogoRitiro) AS conteggio INNER JOIN luogoritiro ON luogoritiro.ID = conteggio.LuogoRitiro ORDER BY TotaleRitiri DESC LIMIT 1;

/*Query che ritorna gli utenti con valori del bilancio non corretti*/
SELECT utente.Username, utente.Email, utente.Nome, utente.Cognome, IF(utente.Classe IS NULL, "No", utente.Classe) AS Studente, utente.Credito, controllo.Bilancio AS CreditoTeorico, SUM(utente.Credito - controllo.Bilancio) AS Differenza FROM (SELECT transazioniOrdini.Utente, SUM(ricarica.TotaleRicaricato - transazioniOrdini.TotalePagato) AS Bilancio FROM (SELECT UtenteRicaricato, SUM(Importo) AS TotaleRicaricato FROM ricarica GROUP BY UtenteRicaricato) as ricarica INNER JOIN (SELECT SUM(Prezzo) AS TotalePagato, ordine.Utente FROM transazione INNER JOIN ordine ON transazione.IDOrdine = ordine.ID GROUP BY Utente) AS transazioniOrdini ON ricarica.UtenteRicaricato = transazioniOrdini.Utente GROUP BY transazioniOrdini.Utente) AS controllo INNER JOIN utente ON controllo.Utente = utente.Username WHERE controllo.Bilancio != utente.Credito GROUP BY utente.Username;

/*Query che controlla se esiste gia un utente con quell'username*/
SELECT * FROM utente WHERE Username = "Username_Da_Testare";

/*Query che controlla se esiste gia un utente con quella email*/
SELECT * FROM utente WHERE Email = "Email_Da_Testare";

/*Query che mostra i prezzi di tutti i prodotti nel DB*/
SELECT prodotto.Descrizione, componenteprezzo.PrezzoProdotto, componenteprezzo.IDProdotto, componenteprezzo.IDFamiglia FROM prodotto INNER JOIN (SELECT *, SUM(componente.Prezzo * componenteprodotto.Quantita) AS PrezzoProdotto FROM componenteprodotto INNER JOIN componente ON componenteprodotto.IDComponente = componente.ID GROUP BY IDFamiglia, IDProdotto) AS componenteprezzo ON ((prodotto.ID = componenteprezzo.IDProdotto) AND (prodotto.IDFamiglia = componenteprezzo.IDFamiglia)) ORDER BY componenteprezzo.IDFamiglia;

/*Query per ottenere tutti i prodotti con relativi prezzi e famiglia con categoria*/
SELECT prodotti.Descrizione, prodotti.PrezzoProdotto, prodotti.IDProdotto, prodotti.IDFamiglia, famiglia.Descrizione AS Famiglia, famiglia.Categoria FROM (SELECT prodotto.Descrizione, componenteprezzo.PrezzoProdotto, componenteprezzo.IDProdotto, componenteprezzo.IDFamiglia FROM prodotto INNER JOIN (SELECT *, SUM(componente.Prezzo * componenteprodotto.Quantita) AS PrezzoProdotto FROM componenteprodotto INNER JOIN componente ON componenteprodotto.IDComponente = componente.ID GROUP BY IDFamiglia, IDProdotto) AS componenteprezzo ON ((prodotto.ID = componenteprezzo.IDProdotto) AND (prodotto.IDFamiglia = componenteprezzo.IDFamiglia)) ORDER BY componenteprezzo.IDFamiglia) AS prodotti INNER JOIN famiglia ON prodotti.IDFamiglia = famiglia.ID;
