USE Bar;

/*Inserimento classi*/

  /*AS 2020-2021
  INSERT INTO classe (Nome) VALUES
  ("1EA"), ("1IA"), ("1IB"), ("1IC"), ("1ID"), ("1IE"), ("1IF"), ("1IG"), ("1MA"),
  ("2EA"), ("2EB"), ("2IA"), ("2IB"), ("2IC"), ("2ID"), ("2IE"), ("2IF"), ("2IG"), ("2IH"), ("2MA"), ("2MB"),
  ("3IA"), ("3IB"), ("3IC"), ("3ID"), ("3IE"), ("3MA"), ("3MB"), ("3MC"), ("3UA"), ("3UB"),
  ("4EA"), ("4IA/MC"), ("4IB"), ("4IC"), ("4ID"), ("4IE"), ("4IF"), ("4MA"), ("4MB"),
  ("5EA"), ("5IA"), ("5IB"), ("5IC/MB"), ("5ID"), ("5IE"), ("5MA"), ("5UA");
  */

  /*Generale*/
  INSERT INTO classe (Nome) VALUES
  ("1EA"), ("1EB"), ("1EC"), ("1ED"), ("1EE"), ("1EF"), ("1EG"), ("1IA"), ("1IB"), ("1IC"), ("1ID"), ("1IE"), ("1IF"), ("1IG"), ("1IH"), ("1MA"), ("1MB"), ("1MC"), ("1MD"), ("1ME"), ("1MF"), ("1MG"), ("1UA"), ("1UB"), ("1UC"), ("1UD"), ("1UE"), ("1UF"), ("1UG"),
  ("2EA"), ("2EB"), ("2EC"), ("2ED"), ("2EE"), ("2EF"), ("2EG"), ("2IA"), ("2IB"), ("2IC"), ("2ID"), ("2IE"), ("2IF"), ("2IG"), ("2IH"), ("2MA"), ("2MB"), ("2MC"), ("2MD"), ("2ME"), ("2MF"), ("2MG"), ("2UA"), ("2UB"), ("2UC"), ("2UD"), ("2UE"), ("2UF"), ("2UG"),
  ("3EA"), ("3EB"), ("3EC"), ("3ED"), ("3EE"), ("3EF"), ("3EG"), ("3IA"), ("3IB"), ("3IC"), ("3ID"), ("3IE"), ("3IF"), ("3IG"), ("3IH"), ("3MA"), ("3MB"), ("3MC"), ("3MD"), ("3ME"), ("3MF"), ("3MG"), ("3UA"), ("3UB"), ("3UC"), ("3UD"), ("3UE"), ("3UF"), ("3UG"),
  ("4EA"), ("4EB"), ("4EC"), ("4ED"), ("4EE"), ("4EF"), ("4EG"), ("4IA"), ("4IB"), ("4IC"), ("4ID"), ("4IE"), ("4IF"), ("4IG"), ("4IH"), ("4MA"), ("4MB"), ("4MC"), ("4MD"), ("4ME"), ("4MF"), ("4MG"), ("4UA"), ("4UB"), ("4UC"), ("4UD"), ("4UE"), ("4UF"), ("4UG"),
  ("5EA"), ("5EB"), ("5EC"), ("5ED"), ("5EE"), ("5EF"), ("5EG"), ("5IA"), ("5IB"), ("5IC"), ("5ID"), ("5IE"), ("5IF"), ("5IG"), ("5IH"), ("5MA"), ("5MB"), ("5MC"), ("5MD"), ("5ME"), ("5MF"), ("5MG"), ("5UA"), ("5UB"), ("5UC"), ("5UD"), ("5UE"), ("5UF"), ("5UG");

/*Inserimento utenti*/

  /*Inserimento utenti generici*/
  INSERT INTO utente (Username, Password, Email, Nome, Cognome) VALUES ("Genitore", "PasswordCriptataGenitore", "emailGenitore@dominio.com", "NomeGenitore", "CognomeGenitore");

  /*Inserimento studenti*/
  INSERT INTO utente (Username, Password, Email, Nome, Cognome, Permesso, Classe) VALUES ("MatricolaStudente","PasswordCriptataStudente","emailStudente@dominio.com","NomeStudente","CognomeStudente","S","1EA"), ("MatricolaStudente2","PasswordCriptataStudente2","emailStudente2@dominio.com","NomeStudente2","CognomeStudente2","S","1EA");

  /*Inserimento Barista*/
  INSERT INTO utente (Username, Password, Email, Nome, Cognome, Permesso) VALUES ("Barista", "PasswordCriptataBarista", "emailBarista@dominio.com", "NomeBarista", "CognomeBarista", 'B');

  /*Inserimento Totem*/
  INSERT INTO utente (Username, Password, Email, Nome, Cognome, Permesso) VALUES ("Totem", "PasswordCriptataTotem", "emailTotem@dominio.com", "NomeTotem", "CognomeTotem", 'T');

/*Inserimento rappresentanti di classe*/
INSERT INTO rappresentanteClasse (Rappresentante, Classe) VALUES ("MatricolaStudente","1EA"), ("MatricolaStudente2","1EA");

/*Inserimento luoghi di ritiro*/
INSERT into luogoRitiro (Luogo) VALUES ("Bar centrale"), ("Ampliamento"), ("Palestra");

/*Inserimento categorie*/
INSERT INTO categoria VALUES ("Tipologia", "Pasto", "Descrizione tipologia");

/*Inserimento famiglia*/
INSERT INTO famiglia VALUES (1, "Prodotto generico", "Tipologia");

/*Inserimento prodotto*/
INSERT INTO prodotto VALUES  (0, 1, "Prodotto principale per Prodotto generico"), (1, 1, "Prodotto alternativo al princiaple per Prodotto generico");

/*Inserimento componente*/
INSERT INTO componente VALUES (1 ,"Nome Componente", 50), (2 ,"Nome Componente2", 50);

/*Inserimento componenti all'interno di un prodotto*/
INSERT INTO componenteProdotto VALUES (1, 0, 1, 1), (1, 0, 2, 1), (1, 1, 1, 1);

/*Creazione ordine*/
INSERT INTO ordine  (LuogoRitiro, OrarioRitiro, Utente) VALUES (1, '2021-05-21 11:00:00', "Genitore");
INSERT INTO prodottoOrdine VALUES (1, 1, 0, 1), (1, 1, 1, 2);
